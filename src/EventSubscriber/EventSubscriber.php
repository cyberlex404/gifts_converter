<?php

namespace Drupal\gifts_converter\EventSubscriber;

use Drupal\gifts_converter\Event\XmlUploadEvent;
use Drupal\gifts_converter\Gifts\Stock;
use Drupal\gifts_converter\Gifts\Tree;
use Drupal\gifts_converter\HandlerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class EventSubscriber.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\gifts_converter\HandlerInterface
   */
  protected $handler;

  /**
   * Constructs a new EventSubscriber object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, HandlerInterface $handler) {
    $this->configFactory = $config_factory;
    $this->handler = $handler;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    //$events['gifts_converter.upload_tree_xml'] = ['onUploadTree'];
    $events['gifts_converter.upload_product_xml'] = ['onUploadProduct'];
    $events['gifts_converter.upload_stock_xml'] = ['onUploadStock'];

    return $events;
  }

  /**
   *
   * @param \Drupal\gifts_converter\Event\XmlUploadEvent $event
   */
  public function onUploadTree(XmlUploadEvent $event) {
    //\Drupal::messenger()->addStatus($event->getFilename());
    //\Drupal::logger('gifts_converter')->debug('Event onUploadTree');
  }

  public function onUploadProduct(XmlUploadEvent $event) {
    $this->handler->handle($event->getFilename(), $event->time());
    //\Drupal::messenger()->addStatus($event->getFilename());
    //\Drupal::logger('gifts_converter')->debug('Event onUploadProduct');
  }

  public function onUploadStock(XmlUploadEvent $event) {
    //\Drupal::logger('gifts_converter')->debug('Event onUploadStock');
    //\Drupal::messenger()->addStatus($event->getFilename());
    $filename = $event->getFilename();
    $stock = new Stock($filename);
    $cache = \Drupal::cache();
    $time = $event->time();
    $items = $stock->getItems();

    if (is_array($items)) {
      foreach ($items as $item) {
        $product_id = $item['product_id'];
        $cid = "gifts:stock:$time:$product_id";
        $cache->set($cid, $item, (time() + 60*60*1));
      }
    }


  }
}
