<?php


namespace Drupal\gifts_converter\Gifts;


class Tree extends GiftsXmlBase {

  public function getPages(int $product)
  {
    $items = [];

    $pages = $this->xml()->xpath("//product[product=$product]/..");
    foreach ($pages as $page) {
      $id = (int) $page->page_id;
      $item = [
        'page_id' => (string) $page->page_id,
        'name' => (string) $page->name,
        'uri' => (string) $page->uri,
        'parent_page_id' => $page->attributes(),
      ];
      $items[$id] = $item;

    }
    return $items;
  }
}