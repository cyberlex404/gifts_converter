<?php


namespace Drupal\gifts_converter\Gifts;


class GiftsProducts extends GiftsXmlBase {

  /**
   * @var array
   */
  protected $items;

  private function record()
  {
    foreach ($this->toArray()['product'] as $item) {
      $this->items[$item['product_id']] = $item;
    }
  }

  public function getItems()
  {
    if (!$this->items) {
      $this->record();
    }
    return $this->items;
  }

  public function products() {
    return $this->toArray()['product'];
  }
}