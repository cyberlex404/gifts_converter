<?php


namespace Drupal\gifts_converter\Gifts;


class Stock extends GiftsXmlBase {

  public function __construct($file) {
    parent::__construct($file);
  }

  protected $items;
  /**
   * @param $id
   *
   * @return \SimpleXMLElement|null
   */
  public function getStock(int $id)
  {
    $search = "//stock[product_id=$id]";
    $value = $this->xml()->xpath($search);
    return isset($value[0]) ? $value[0]: null;
  }

  /**
   * @param int $id
   * @param string $value
   *
   * @return \SimpleXMLElement|null
   */
  private function getXmlValue(int $id, string $value = 'amount')
  {
    $search = "//stock[product_id=$id]/$value";
    $value = $this->xml()->xpath($search);
    return isset($value[0]) ? $value[0]: null;
  }

  public function getItems()
  {
    if (!$this->items) {
      $this->record();
    }
    return $this->items;
  }

  function record()
  {
    foreach ($this->toArray()['stock'] as $item) {
      $this->items[$item['product_id']] = $item;
    }
  }

  /**
   * @param int $id
   *
   * @return mixed
   */
  public function getItem(int $id)
  {
    return $this->getItems()[$id];
  }

  /**
   * @param int $id
   *
   * @return int
   */
  public function getAmount(int $id) {
    $item = $this->getItem($id);

    if (!empty($item) && isset($item['amount'])) {
      return (int) $item['amount'];
    }else{
      return 0;
    }
  }

}