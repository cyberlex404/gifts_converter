<?php

namespace Drupal\gifts_converter\Gifts;

use Drupal\gifts_converter\GiftsXmlInterface;

class GiftsXmlBase implements GiftsXmlInterface {

  protected $file;

  protected $xml;

  /**
   * GiftsXmlBase constructor.
   *
   * @param string $file
   */
  public function __construct($file) {
    $this->file = $file;
  }

  /**
   * @return \SimpleXMLElement|null
   */
  public function xml()
  {
    if (!$this->xml) {
      try {
        $this->xml = simplexml_load_file($this->file);
      }catch (\Exception $e) {
        \Drupal::messenger()->addError($e->getMessage());
        \Drupal::logger('gifts_converter')->error($e->getMessage());
      }

    }
    return $this->xml;
  }

  /**
   * @return false|string
   */
  public function json()
  {
    return json_encode($this->xml());
  }

  /**
   * @return mixed
   */
  public function toArray()
  {
    return json_decode($this->json(),TRUE);
  }

}