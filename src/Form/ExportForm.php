<?php

namespace Drupal\gifts_converter\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gifts_converter\Gifts\Stock;
use Drupal\gifts_converter\Gifts\Tree;
use Drupal\gifts_converter\HandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\gifts_converter\XmlUploadInterface;
use Drupal\gifts_converter\ExportInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class ExportForm.
 */
class ExportForm extends FormBase {

  /**
   * Drupal\gifts_converter\XmlUploadInterface definition.
   *
   * @var \Drupal\gifts_converter\XmlUploadInterface
   */
  protected $giftsConverterXml;
  /**
   * Drupal\gifts_converter\ExportInterface definition.
   *
   * @var \Drupal\gifts_converter\ExportInterface
   */
  protected $giftsConverterExport;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\gifts_converter\HandlerInterface
   */
  protected $handler;
  /**
   * Constructs a new ExportForm object.
   */
  public function __construct(
    XmlUploadInterface $gifts_converter_xml,
    ExportInterface $gifts_converter_export,
    HandlerInterface $handler,
    ConfigFactoryInterface $config_factory
  ) {
    $this->giftsConverterXml = $gifts_converter_xml;
    $this->giftsConverterExport = $gifts_converter_export;
    $this->configFactory = $config_factory;
    $this->handler = $handler;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('gifts_converter.xml'),
      $container->get('gifts_converter.export'),
      $container->get('gifts_converter.handler'),
      $container->get('config.factory')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $item = new FormattableMarkup("Queue @count", [
      '@count' => \Drupal::service('queue')->get('gifts_converter_handler')->numberOfItems(),
    ]);
    $form['queue_info'] = [
      '#type' => 'item',
      '#markup' => $item,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $last = \Drupal::state()->get('gifts_converter.last_transaction');

    $uri = 'public://gifts_converter/' . $last . '/product.xml';
    $file = \Drupal::service('file_system')->realpath($uri);
    $this->handler->handle($file, $last);
  }

  protected function getCategory($productID, $time, Tree &$tree) {

    $cacheS = \Drupal::cache();
    $cid = "gifts:tree:$time:$productID";

    if ($cache = $cacheS->get($cid)) {
      $category = $cache->data;
    }else{
      $pages = $tree->getPages($productID);
      $category = [];
      foreach ($pages as $item) {
        $category[] = $item['name'];
      }
      $cacheS->set($cid, $category, (time() + 60*60*72));
    }
    return $category;

  }

}
