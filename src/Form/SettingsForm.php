<?php

namespace Drupal\gifts_converter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gifts_converter\GiftsXmlInterface;
use Drupal\gifts_converter\XmlUploadInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gifts_converter.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gifts_converter.settings');

    $form['access'] = [
      '#type' => 'details',
      '#title' => $this->t('Gifts.ru access'),
      '#tree' => TRUE,
    ];
    $form['access']['login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login'),
      '#default_value' => $config->get('access.login'),
    ];
    $form['access']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('access.password'),
    ];

    $form['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Delay'),
      '#default_value' => $config->get('delay'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\gifts_converter\XmlUpload $xml */
    $xml = \Drupal::service('gifts_converter.xml');



    $this->config('gifts_converter.settings')
      ->set('access', $form_state->getValue('access'))
      ->set('delay', $form_state->getValue('delay'))
      ->save();
  }

}
