<?php

namespace Drupal\gifts_converter;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\gifts_converter\Gifts\Stock;
use Drupal\gifts_converter\Gifts\Tree;
use Exception;

/**
 * Class Export.
 */
class Export implements ExportInterface, GiftsXmlInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a new Export object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->cache = $cache = \Drupal::cache();
  }

  public function createItem($itemData, $mode = 'csv') {

    $start = microtime(true);

    $data = $itemData['product'];
    $xml = $itemData['xml'];
    $filename = $itemData['file'];
    $time = $itemData['time'];

    $access = $this->configFactory->get('gifts_converter.settings')->get('access');

    $productID = (int)  $data['product_id'];
    // sku;post_title;post_content;category;stock_status;weight;size_code;matherial;regular_price;featured_image;product_gallery

    $attachment = [];

    $attachment_prefix = 'http://' . $access['login'] . ':' . $access['password'] . self::API_URL;
    if (is_array($data['product_attachment'])) {
      foreach ($data['product_attachment'] as $item) {

        try {
          if (isset($item['meaning']) && $item['meaning'] == '1') {
            if (isset($item['image'])) {
              $attachment[] = $attachment_prefix . $item['image'];
            }
          }
        }catch (\Exception $e) {
          \Drupal::logger('attachment error')->warning("Error in record ID: $productID");
        }

      }
    }

    $category = $this->getCategory($productID, $time, $xml['tree']);

   // \Drupal::logger('gifts_converter')->debug(Json::encode( $data['content']));
    try {
      $body = !empty($data['content']) ? $data['content'] : '';


      $body = $this->clearContent($body);

      if (gettype($body) == 'array') {
        \Drupal::logger('gifts_converter_body')->debug(Json::encode($body));
      }else{
        \Drupal::logger('gifts_converter_body')->debug("$productID: content: $body");
      }


    }catch (Exception $exception) {
      \Drupal::logger('gifts_converter')->critical('Error in '. (string) $productID . ' '. $exception->getMessage());
      \Drupal::logger('gifts_converter')->critical(Json::encode($data));
    }

    $cid = "gifts:stock:$time:$productID";

    $cache = \Drupal::cache();

    if ($cacheData = $cache->get($cid)) {
      $stock_amount = $cacheData->data['amount'];
    }else{
      $stock = new Stock($xml['stock']);
      $stock_amount = $stock->getAmount($productID);
    }


    $item = [
      'product_id' => $data['product_id'],
      'sku' => $data['code'],
      'post_title' => $data['name'],
      'post_content' => $body, // todo: удалить ссылки TEST
      'category' => implode("|",$category),
      'stock_amount' => $stock_amount,
      'weight' => $data['weight'],
      'size_code' => $data['product_size'],
      'matherial' => $data['matherial'],
      'regular_price' => $data['price']['price'],
      'featured_image' => $attachment_prefix . $data['small_image']['@attributes']['src'],
      'product_gallery' => implode("|",$attachment),
    ];

    if ($mode == 'csv') {

      try {
        $file = fopen($filename, 'a');
        fputcsv($file, $item, ';');
        fclose($file);
      }catch (Exception $e) {
        echo 'Error in csv file record';
      }

    }

    $name = $data['name'];
    $finish = microtime(true);
    $delta = $finish - $start;
    if($productID < 3500) {
      \Drupal::logger('debug')->notice("record in $filename ID: $productID Name: $name (time: $delta)");
    }
    //
    // TODO: Implement createItem() method.
  }


  /**
   * @param string $string
   *
   * @return string|string[]|null
   */
  public function clearContent($string = '') {
    if (empty($string)) {
      return '';
    }
    $pattern[0] = "/<(\/*(a|A)\W).*?>/";
    $pattern[1] = "/(<\/(a|A)>)/";
    $replacements[0] = '';
    $replacements[1] = '';
    $result = preg_replace($pattern, [], $string);

    if (empty($result)) {
      return '';
    }elseif (is_array($result)) {
      return '';
    }else{
      return $result;
    }
  }

  /**
   * @param $productID
   * @param $time
   * @param string
   *
   * @return array
   */
  protected function getCategory($productID, $time, &$treeFile) {


    $cid = "gifts:tree:$productID";

    if ($cache = $this->cache->get($cid)) {
      $category = $cache->data;
    }else{
      $tree = new Tree($treeFile);
      $pages = $tree->getPages($productID);
      $category = [];
      foreach ($pages as $item) {
        $category[] = $item['name'];
      }
      $this->cache->set($cid, $category);
    }
    return $category;

  }


}
