<?php

namespace Drupal\gifts_converter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\gifts_converter\Event\XmlUploadEvent;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class XmlUpload.
 */
class XmlUpload implements XmlUploadInterface, GiftsXmlInterface {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new XmlUpload object.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, FileSystemInterface $fileSystem) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritDoc}
   */
  public function upload($file, $dir = 'gifts') {
    $access = $this->configFactory->get('gifts_converter.settings')->get('access');
    $url = 'http://' . $access['login'] . ':' . $access['password'] . self::API_URL . $file;

    $fileUri = 'public://gifts_converter/' . $dir;

    $events = [
      GiftsXmlInterface::XML_TREE => XmlUploadEvent::XML_TREE_EVENT,
      GiftsXmlInterface::XML_STOCK => XmlUploadEvent::XML_STOCK_EVENT,
      GiftsXmlInterface::XML_PRODUCT => XmlUploadEvent::XML_PRODUCT_EVENT,
    ];
    $log = t('Dir @dir', [
      '@dir'=> $fileUri,
    ]);
    \Drupal::logger('gifts_converter')->notice($log);
    $realpath = $this->fileSystem->realpath($fileUri);
    $savePath = $realpath . '/' . $file;
    try{
      $response = $this->httpClient->request('GET', $url, ['sink' => $savePath]);
      if ($response instanceof ResponseInterface &&
        $response->getStatusCode() == 200 &&
        file_exists($savePath)
      ) {
        $dispatcher = \Drupal::service('event_dispatcher');
        // Create event object passing arguments.
        $event = new XmlUploadEvent($savePath, $file, $dir);

        $dispatcher->dispatch($events[$file], $event);

        return $savePath;
      }
    }catch (RequestException $e) {
      \Drupal::logger('gifts_converter')->error($e->getMessage());
      \Drupal::messenger()->addError($e->getMessage());
    }catch (\Exception $e) {
      \Drupal::logger('gifts_converter')->error($e->getMessage());
    }
  }

  public function prepare($time) {
    $dir = (string) $time;
    $path = 'public://gifts_converter/' . $dir;
    if (!file_exists($this->fileSystem->realpath($path))) {
      $this->fileSystem->mkdir('public://gifts_converter/'. $dir, NULL, TRUE);
    }

    $files = [
      self::XML_TREE,
      self::XML_STOCK,
      self::XML_PRODUCT,
    ];

    /** @var  \Drupal\Core\Queue\QueueFactory $queueFactory */
    $queueFactory = \Drupal::service('queue');

    $queue = $queueFactory->get('gifts_converter_xml');

    foreach ($files as $item) {
      $queue->createItem([
        'file' => $item,
        'dir' => $dir,
      ]);
    }

  }

}
