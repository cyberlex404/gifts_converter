<?php


namespace Drupal\gifts_converter;


interface GiftsXmlInterface {

  const API_URL = '@api2.gifts.ru/export/v2/catalogue/';

  const XML_TREE = 'tree.xml';
  const XML_PRODUCT = 'product.xml';
  const XML_CATALOGUE = 'catalogue.xml';
  const XML_STOCK = 'stock.xml';
  const XML_TREE_WITHOUT = 'treeWithoutProducts.xml';
  const XML_TREE_FILTERS = 'filters.xml';

  const XML_DIR_URI = 'public://gifts_converter/';
}