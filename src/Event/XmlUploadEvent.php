<?php

namespace Drupal\gifts_converter\Event;

use Symfony\Component\EventDispatcher\Event;

class XmlUploadEvent extends Event {

  const XML_TREE_EVENT = 'gifts_converter.upload_tree_xml';

  const XML_PRODUCT_EVENT = 'gifts_converter.upload_product_xml';

  const XML_STOCK_EVENT = 'gifts_converter.upload_stock_xml';

  protected $filename;

  protected $type;

  protected $time;

  /**
   * XmlUploadEvent constructor.
   *
   * @param string $filename
   * @param string $type
   * @param string $time
   */
  public function __construct($filename, $type, $time) {
    $this->filename = $filename;
    $this->type = $type;
    $this->time = $time;
  }

  /**
   * @return string
   */
  public function getFilename() {
    return $this->filename;
  }

  public function time() {
    return $this->time;
  }

  public function type() {
    return $this->type;
  }
}