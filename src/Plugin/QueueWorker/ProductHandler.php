<?php


namespace Drupal\gifts_converter\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\gifts_converter\ExportInterface;
use Drupal\gifts_converter\XmlUploadInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProductHandler
 *
 *  * @QueueWorker(
 *   id = "gifts_converter_handler",
 *   title = "Product handler worker",
 *   cron = {"time" = 60}
 * )
 *
 * @package Drupal\gifts_converter\Plugin\QueueWorker
 */
class ProductHandler extends QueueWorkerBase implements ContainerFactoryPluginInterface{

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\gifts_converter\ExportInterface
   */
  protected $export;
  /**
   * ProductHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(ConfigFactoryInterface $config_factory, ExportInterface $export) {
    $this->configFactory = $config_factory;
    $this->export = $export;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return \Drupal\Core\Plugin\ContainerFactoryPluginInterface|\Drupal\gifts_converter\Plugin\QueueWorker\XmlUpload
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory'),
      $container->get('gifts_converter.export')
    );
  }

  public function processItem($data) {

    $this->export->createItem($data);

  }
}