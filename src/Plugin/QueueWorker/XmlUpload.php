<?php

namespace Drupal\gifts_converter\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\gifts_converter\XmlUploadInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class XmlUpload
 *
 * @QueueWorker(
 *   id = "gifts_converter_xml",
 *   title = "Upload XML file",
 *   cron = {"time" = 60}
 * )
 * @package Drupal\gifts_converter\Plugin\QueueWorker
 */
class XmlUpload extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\gifts_converter\XmlUploadInterface
   */
  protected $xmlUpload;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * XmlUpload constructor.
   *
   * @param \Drupal\gifts_converter\XmlUploadInterface $xmlUpload
   */
  public function __construct(XmlUploadInterface $xmlUpload, ConfigFactoryInterface $config_factory) {
    $this->xmlUpload = $xmlUpload;
    $this->configFactory = $config_factory;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return \Drupal\Core\Plugin\ContainerFactoryPluginInterface|\Drupal\gifts_converter\Plugin\QueueWorker\XmlUpload
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('gifts_converter.xml'),
      $container->get('config.factory')
    );
  }

  public function processItem($data) {

    $file = $data['file'];
    if ($realpath = $this->xmlUpload->upload($data['file'], $data['dir'])) {
      $log = t('XML-file upload to @file', ['@file' => 'public://gifts_converter/' . $data['dir']. '/'.$data['file']]);
      //\Drupal::messenger()->addStatus($log);
      \Drupal::logger('gifts_converter')->notice($log);
      //$this->configFactory->getEditable('gifts_converter.settings')->set('files.'. $file . time(), $realpath);
    }

  }


}