<?php

namespace Drupal\gifts_converter;

/**
 * Interface ExportInterface.
 */
interface ExportInterface {

  /**
   * @param $data
   * @param $file
   *
   * @return mixed
   */
  public function createItem($data, $mode = 'csv');
}
