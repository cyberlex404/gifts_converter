<?php

namespace Drupal\gifts_converter;

use DOMDocument;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\gifts_converter\Gifts\GiftsProducts;
use Drupal\gifts_converter\Gifts\Stock;
use Drupal\gifts_converter\Gifts\Tree;

/**
 * Class Handler.
 */
class Handler implements HandlerInterface, GiftsXmlInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;
  /**
   * Constructs a new Handler object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $fileSystem,
    QueueFactory $queueFactory
  ) {
    $this->configFactory = $config_factory;
    $this->fileSystem = $fileSystem;
    $this->queueFactory = $queueFactory;
  }
//\Drupal\Core\Queue\QueueFactory $queueFactory

  /**
   * @param $xmlProductPath
   * @param $last_transaction
   *
   * @throws \Exception
   */
  public function handle($xmlProductPath, $last_transaction) {

    $uri = self::XML_DIR_URI . $last_transaction;
    if (!file_exists($xmlProductPath)) {
      throw new \Exception('XML file product.xml not found in directory');
    }

    $realpath = $this->fileSystem->realpath($uri);
    if (!file_exists($realpath)){
      throw new \Exception('Directory not found');
    }

    if (!file_exists($realpath . '/' . self::XML_TREE)) {
      throw new \Exception('XML file tree.xml not found in directory');
    }

    if (!file_exists($realpath . '/' . self::XML_STOCK)) {
      throw new \Exception('XML file stock.xml not found in directory');
    }

    $exportFile = $realpath . '/goods.csv';

    if ($file = fopen($exportFile, 'w')) {

      $item = [
        'product_id',
        'sku',
        'post_title',
        'post_content',
        'category',
        'stock_amount',
        'weight',
        'size_code',
        'matherial',
        'regular_price',
        'featured_image',
        'product_gallery',
      ];
      fputcsv($file, $item, ';');
      fclose($file);
      $queue = $this->queueFactory->get('gifts_converter_handler');
      $products = new GiftsProducts($xmlProductPath);

      $handleItems = $products->toArray()['product'];

      //$handleItems = array_slice($products->toArray()['product'], 0, 30);

      foreach ($handleItems as $productItem) {
        $queueItem = [
          'product' => $productItem,
          'file' => $exportFile,
          'xml' => [
            'tree' => $realpath . '/' . self::XML_TREE,
            'stock' => $realpath . '/' . self::XML_STOCK,
          ],
          'time' => $last_transaction,
        ];
        $queue->createItem($queueItem);
      }
      $log = t('created a queue of @count items', [
        '@count' => $queue->numberOfItems(),
      ]);
      \Drupal::messenger()->addStatus($log);
    }
  }

  public function createCsv($exportFile) {
    fopen($exportFile, 'a');
  }

  /**
   * @param $realpath
   *
   * @return int
   * @throws \Exception
   */
  public function createFile($exportFile) {
    $time = new \DateTime();
    $xml = new DOMDocument('1.0','utf-8');
    $root = $xml->appendChild($xml->createElement('products'));
    $docTime = $xml->createAttribute('timestamp');
    $docTime->value = $time->format('r');
    $root->appendChild($docTime);

    $product = $xml->createElement('start');
    $product->appendChild($xml->createTextNode('start'));
    $root->appendChild($product);
    $xml->formatOutput = true;
    return $xml->save($exportFile);
  }

  public function stock($dir) {

  }

}
