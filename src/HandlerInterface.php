<?php

namespace Drupal\gifts_converter;

/**
 * Interface HandlerInterface.
 */
interface HandlerInterface {

  /**
   * @param null $file
   *
   * @return mixed
   */
  public function handle($file, $time);
}
