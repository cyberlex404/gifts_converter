<?php

namespace Drupal\gifts_converter;

/**
 * Interface XmlUploadInterface.
 */
interface XmlUploadInterface {

  /**
   * @param $file string
   * @param $dir string
   *
   * @return mixed
   */
  public function upload(string $file, $dir);
}
